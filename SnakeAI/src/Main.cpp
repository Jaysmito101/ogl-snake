#define _CRT_SECURE_NO_WARNINGS
#include <Base.h>
#include "FrameBuffer.h"
#include <implot/implot.h>
#include <EntryPoint.h>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <string>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext/matrix_relational.hpp>
#include <glm/ext/vector_relational.hpp>
#include <glm/ext/scalar_relational.hpp>

#include <time.h>
#include <string.h>
#include <mutex>
#include <sys/stat.h>
#include <dirent/dirent.h>
#include <atomic>

#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui_internal.h"

#include "State.h"
#include "game/GridRenderer.h"
#include "game/SnakeGame.h"
#include "game/UI.h"

static Application* myApp;

// This is optional code for enabling the dockspace. This is not included within ogl-core as for most purposes it will not be used.
static void OnBeforeImGuiRender() {

	static bool dockspaceOpen = true;
	static bool opt_fullscreen_persistant = true;
	bool opt_fullscreen = opt_fullscreen_persistant;
	static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
	if (opt_fullscreen)
	{
		ImGuiViewport* viewport = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.1f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
	}
	if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
		window_flags |= ImGuiWindowFlags_NoBackground;
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("DockSpace Demo", &dockspaceOpen, window_flags);
	ImGui::PopStyleVar();
	if (opt_fullscreen) {
		ImGui::PopStyleVar(2);
	}
	// DockSpace
	ImGuiIO& io = ImGui::GetIO();
	ImGuiStyle& style = ImGui::GetStyle();
	float minWinSizeX = style.WindowMinSize.x;
	style.WindowMinSize.x = 370.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
	{
		ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
	}

	style.WindowMinSize.x = minWinSizeX;

	//ShowMenu();
}

static void OnImGuiRenderEnd() {
	ImGui::End();
}


#include <iostream>
class MyApp : public Application
{
public:
	virtual void OnPreload() override {
		SetTitle("SnakeAI - Jaysmito Mukherjee");
		
	}

	virtual void OnUpdate(float deltatime) override
	{
		state.deltatime = deltatime;
		static float dtime = 0.0f;
		if (dtime >= state.frameDelta)
		{
			if (state.snakeGame->IsPlaying())
				state.snakeGame->UpdateLogic();
			dtime = 0.0f;
		}
		
		state.snakeGame->UpdateGraphics();
		
		dtime += deltatime;

		state.framebuffer->Begin();
		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		state.gridRenderer->Render();


		state.framebuffer->End();

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		RenderImGui();


		// input
		if (ImGui::IsKeyDown(ImGuiKey_UpArrow))
			state.snakeGame->SetDirection(Up);
		else if (ImGui::IsKeyDown(ImGuiKey_DownArrow))
			state.snakeGame->SetDirection(Down);
		else if (ImGui::IsKeyDown(ImGuiKey_RightArrow))
			state.snakeGame->SetDirection(Right);
		else if (ImGui::IsKeyDown(ImGuiKey_LeftArrow))
			state.snakeGame->SetDirection(Left);
	}

	virtual void OnOneSecondTick() override
	{

	}

	virtual void OnImGuiRender() override
	{
		OnBeforeImGuiRender();
		
		// The UI & stats
		state.ui->Render();

		// The Game Viewport
		ImGui::Begin("Game");
		ImGui::Image(reinterpret_cast<ImTextureID>(state.framebuffer->GetColorTexture()), ImVec2(512, 512));
		ImVec2 beginRect = ImGui::GetItemRectMin();
		ImVec2 size = ImGui::GetItemRectSize();
		ImVec2 mousePos = (ImGui::GetMousePos() - beginRect) / size;
		state.mouse[0] = mousePos.x;
		state.mouse[1] = mousePos.y;
		ImGui::End();

		OnImGuiRenderEnd();
	}

	virtual void OnStart(std::string loadFile) override
	{
		srand((unsigned int)time(NULL));
		GetWindow()->SetShouldCloseCallback([this](int, int)->void { Close(); });

		// Create state
		state.gridRenderer = new GridRenderer(16, &state);
		state.ui = new UI(&state);
		state.snakeGame = new SnakeGame(&state);
		state.framebuffer = new FrameBuffer(1024, 1024);
	}

	void OnEnd()
	{
		// Destroy State
		delete state.gridRenderer;
		delete state.ui;
		delete state.snakeGame;
		delete state.framebuffer;
	}

private:
	State state;
};

Application* CreateApplication()
{
	myApp = new MyApp();
	return myApp;
}
