#pragma once
#include <vector>

enum CellState
{
	Wall = 0,
	SnakeHead,
	SnakeBody,
	Fruit,
	Empty,
	DeadSnake
};

enum Direction
{
	Up = 0,
	Down,
	Left,
	Right
};

struct State;



class Snake
{
public:
	Snake(int x, int y);
	~Snake();

	void Grow();
	void Move(Direction dir);

	inline std::vector<std::pair<int, int>>& GetBody() { return body; }

private:
	std::vector<std::pair<int, int>> body;
};

class SnakeGame
{
public:
	SnakeGame(State* state);
	~SnakeGame();

	bool UpdateLogic();
	void UpdateGraphics();

	bool UpdateCellStates();
	void NewFruit();

	void Reset();
		
	inline bool IsPlaying() { return this->isPlaying; }
	inline SnakeGame* SetDirection(Direction dir) { this->dir = dir; return this; };
	inline int GetScore() { return score; }
	inline int GetLifespan() { return lifespan; }

	std::pair<int, int> fruit = {-1, -1};
private:
	State* state = nullptr;
	Direction dir = Up;
	CellState* cells = nullptr;	
	Snake snake = Snake(0, 0);
	int gridSize = 0;
	bool isPlaying = true;
	int lifespan = 0;
	int score = 0;
};