#include "GridRenderer.h"
#include "../State.h"


#include <glm/gtc/constants.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext/matrix_relational.hpp>
#include <glm/ext/vector_relational.hpp>
#include <glm/ext/scalar_relational.hpp>

static const float vertices[] = {
	-1.0,  1.0, 0.0,
	 1.0, -1.0, 0.0,
	-1.0, -1.0, 0.0,
	 1.0,  1.0, 0.0
};

static const unsigned int indices[] = {
	0, 2, 1,
	0, 1, 3
};

static const char* vertexShader = R"(
#version 430 core
layout (location = 0) in vec3 aPos;

struct GridItem
{
	vec4 color;
	vec4 position;
	vec4 hovered;
	ivec4 id;
};

layout(std430, binding = 0) buffer GridItems
{
	GridItem grid_items[];
};

out vec4 color;
out vec4 fragPos;
out flat int instanceID;

uniform mat4 projection;
uniform mat4 view;

uniform float scale = 0.05f;
uniform vec3 offset = vec3(0.0f);

mat4 scale_mat(float c)
{
	return mat4(c, 0, 0, 0,
	            0, c, 0, 0,
	            0, 0, c, 0,
	            0, 0, 0, 1);
}


void main()
{
	vec3 position = grid_items[gl_InstanceID].position.xyz + offset;
	color = grid_items[gl_InstanceID].color;
	instanceID = gl_InstanceID;
	mat4 model_matrix = scale_mat(scale);
	model_matrix[3] = model_matrix[0] * position.x + model_matrix[1] * position.y + model_matrix[2] * position.z + model_matrix[3];
    gl_Position = projection * view * model_matrix * vec4(aPos, 1.0f); 
	fragPos = gl_Position;
}
)";

static const char* fragmentShader = R"(
#version 430 core

in vec4 color;
in vec4 fragPos;
in flat int instanceID;

out vec4 FragColor;

uniform vec3 mouse;

struct GridItem
{
	vec4 color;
	vec4 position;
	vec4 hovered;
	ivec4 id;
};

layout(std430, binding = 0) buffer GridItems
{
	GridItem grid_items[];
};

const float MOUSE_THRESHOLD = 0.005;

void main()
{
	vec2 uv = gl_FragCoord.xy / 1024.0f;
	float dst = length(uv - mouse.xy);
	if(dst < MOUSE_THRESHOLD)	
	{
		grid_items[instanceID].hovered = vec4(1.0f);
		//grid_items[instanceID].color = vec4(vec3(0.0f), 1.0f);
		FragColor = vec4(vec3(0.0f), 1.0f);
		return;
	}

    FragColor = color;
	//FragColor = vec4(vec3(fragPos.x, fragPos.y, 0.0), 1.0f);
}
)";
 
GridRenderer::GridRenderer(int size, State* state)
{
	this->state = state;

	shader = new Shader(vertexShader, fragmentShader);

	// Setup GPU Quad Mesh
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void*)0);
	glEnableVertexAttribArray(0);

	// Setup SSBO

	glGenBuffers(1, &ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GridItem) * size * size, NULL, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	// Setup camera
	projection = glm::ortho(-1, 1, -1, 1);
	view = glm::lookAt(cameraPosition, cameraPosition + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	// Setup grid items
	this->gridSize = size;
	UpdateGrid();
}

GridRenderer::~GridRenderer()
{
	delete shader;
	delete[] grid;

	// Destroy GPU Quad Mesh
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ssbo);
	glDeleteBuffers(1, &ebo);
}

void GridRenderer::Resize(int newSize)
{
	this->gridSize = newSize;
	UpdateGrid();
}

void GridRenderer::Render()
{
	shader->Bind();
	shader->SetUniformMat4("projection", projection);
	shader->SetUniformMat4("view", view);
	shader->SetUniformf("scale", scale * state->scale * 0.5);
	shader->SetUniform3f("offset", state->position);
	shader->SetUniform3f("mouse", state->mouse);

	if (hoveredItem > -1)
	{
		UpdateGrid();
		grid[hoveredItem].color[0] = 0.0f;
		grid[hoveredItem].color[1] = 0.0f;
		grid[hoveredItem].color[2] = 0.0f;
		grid[hoveredItem].color[2] = 1.0f;
	}

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GridItem) * gridSize * gridSize, grid, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	
	glBindVertexArray(vao);
	//glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
	glDrawElementsInstanced(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0, gridSize * gridSize);
	glBindVertexArray(0);
	shader->Unbind();

	UpdateMousePicker();
}

void GridRenderer::UpdateGrid()
{
	static int32_t oldSize = 0;

	if (oldSize != gridSize)
	{
		scale = 2.0f / static_cast<float>(gridSize);


		if (grid)
			delete[] grid;

		grid = new GridItem[gridSize * gridSize];
		oldSize = gridSize;
	}
	for (int y = 0; y < gridSize; y++)
	{
		for (int x = 0; x < gridSize; x++)
		{
			int index = y * gridSize + x;
			grid[index] = GridItem();
			grid[index].position[0] = -1 * (gridSize - 1) + 2 * x;
			grid[index].position[1] = -1 * (gridSize - 1) + 2 * y;
			grid[index].position[2] = 0.0f;

			grid[index].color[0] = (static_cast<float>(x) / gridSize);
			grid[index].color[1] = (static_cast<float>(y) / gridSize);

			grid[index].id[0] = x;
			grid[index].id[1] = y;
			grid[index].id[3] = gridSize;
		}

	}
}

void GridRenderer::UpdateMousePicker()
{
	// TODO: Implement
	//std::vector<GridItem> storage(gridSize * gridSize);
	//glGetNamedBufferSubData(ssbo, 0, gridSize * gridSize * sizeof(GridItem), storage.data());
	//
	//int tmp = 0;
	//for (auto& gi : storage)
	//{
	//	if (gi.hovered[0] > 0.5)
	//	{
	//		hoveredItem = tmp;
	//		break;
	//	}
	//	tmp++;
	//}
	//hoveredItem = -1;
}
