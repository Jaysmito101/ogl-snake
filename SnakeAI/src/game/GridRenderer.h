#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <Base.h>
#include <implot/implot.h>
#include <EntryPoint.h>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <string>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext/matrix_relational.hpp>
#include <glm/ext/vector_relational.hpp>
#include <glm/ext/scalar_relational.hpp>

struct State;


struct GridItem
{
	float color[4] = {1.0f};
	float position[4] = {0.0f};
	float hovered[4] = {0.0f};
	int id[4] = {0};
};

/*
* Renders a square griid of Quads
*/
class GridRenderer
{
public:
	GridRenderer(int size, State* state);
	~GridRenderer();

	void Resize(int newSize);

	void Render();

	void UpdateGrid();

	void UpdateMousePicker();

	inline void Set(int x, int y, float r = 0.0f, float g = 0.0f, float b = 0.0f)
	{
		int index = y * gridSize + x;
		grid[index].color[0] = r;
		grid[index].color[1] = g;
		grid[index].color[2] = b;
	}

private:
	State* state = nullptr;
	Shader* shader = nullptr;
	uint32_t vao = 0, ebo = 0, vbo = 0, ssbo = 0;
	int32_t gridSize = 0;
	float scale = 1.0f;

	glm::mat4 projection, view;
	glm::vec3 cameraPosition = glm::vec3(0.0f, 0.0f, -1.0f);

	GridItem* grid = nullptr;

	int hoveredItem = -1;
};