#pragma once

#include "imgui/imgui.h"

struct State;

class UI
{
public:
	UI(State* state);
	~UI();

	void Render();

private:
	State* state;
};
