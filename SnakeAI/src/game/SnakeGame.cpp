#include "SnakeGame.h"
#include "../State.h"
#include "GridRenderer.h"

SnakeGame::SnakeGame(State* state)
{
	this->state = state;
	Reset();
}

SnakeGame::~SnakeGame()
{
	if (cells)
		delete[] cells;
}

void SnakeGame::UpdateGraphics()
{
	for (int y = 0; y < gridSize; y++)
	{
		for (int x = 0; x < gridSize; x++)
		{
			int index = y * gridSize + x;
			switch (cells[index])
			{
				case Wall:			state->gridRenderer->Set(x, y, 0.1f, 0.1f, 0.1f); break;
				case SnakeHead:		state->gridRenderer->Set(x, y, 0.0f, 0.7f, 0.5f); break;
				case SnakeBody:		state->gridRenderer->Set(x, y, 0.4f, 0.4f, 0.4f); break;
				case Fruit:			state->gridRenderer->Set(x, y, 0.0f, 0.0f, 1.0f); break;
				case Empty:			state->gridRenderer->Set(x, y, 0.2f, 0.2f, 0.2f); break;
				case DeadSnake:		state->gridRenderer->Set(x, y, 1.0f, 0.0f, 0.0f); break;
			}
		}
	}
}

bool SnakeGame::UpdateCellStates()
{
	for (int y = 0; y < gridSize; y++)
	{
		for (int x = 0; x < gridSize; x++)
		{
			int index = y * gridSize + x;
			if (x == 0 || x == gridSize - 1 || y == 0 || y == gridSize - 1)
			{
				cells[index] = Wall;
			}
			else if (x == fruit.first && y == fruit.second)
			{
				cells[index] = Fruit;
			}
			else
			{
				cells[index] = Empty;
			}
		}
	}

	bool head = true;
	for (auto& bodyPart : snake.GetBody())
	{
		if (bodyPart.first < 0 || bodyPart.second < 0 || bodyPart.first > gridSize - 1 || bodyPart.second > gridSize - 1)
			return false;

		int index = 0;
		index = bodyPart.second * gridSize + bodyPart.first;
		if (cells[index] != Empty && cells[index] != Fruit)
		{
			cells[index] = DeadSnake;
		}
		else
		{
			if (head)
			{
				cells[index] = SnakeHead;
				head = false;
			}
			else
				cells[index] = SnakeBody;
		}
	}
}

void SnakeGame::NewFruit()
{
	int fx = rand() % (state->gridSize-2) + 1;
	int fy = rand() % (state->gridSize-2) + 1;

	fruit.first = fx;
	fruit.second = fy;
}

void SnakeGame::Reset()
{
	if (gridSize != state->gridSize)
	{
		if (cells)
			delete cells;

		gridSize = state->gridSize;
		cells = new CellState[gridSize * gridSize];
	}

	gridSize = state->gridSize;
	score = 0;
	lifespan = 0;
	snake = Snake(state->gridSize / 2, state->gridSize / 2); 
	isPlaying = true;
	NewFruit();
	UpdateCellStates();
}

bool SnakeGame::UpdateLogic()
{
	lifespan++;
	snake.Move(dir);
	if (!UpdateCellStates())
	{
		isPlaying = false;
		return false;
	}

	auto& head = snake.GetBody()[0];
	int index = head.second * gridSize + head.first;
	if (head == fruit)
	{
		snake.Grow();
		score++;
		NewFruit();
	}
	else if (cells[index] != SnakeHead)
	{
		isPlaying = false;
		return false;
	}
	return true;
}

Snake::Snake(int x, int y)
{
	body.push_back({ x, y });
}

Snake::~Snake()
{
	body.clear();
}

void Snake::Grow()
{
	body.push_back(body.back());
}

void Snake::Move(Direction dir)
{
	int xp = 0;
	int yp = 0;
	switch (dir)
	{
	case Up:
		yp = -1;
		break;
	case Down:
		yp = 1;
		break;
	case Right:
		xp = -1;
		break;
	case Left:
		xp = 1;
		break;
	default:
		break;
	}

	auto head = body[0];

	if (body.size() > 1)
	{
		std::pair<int, int> tmp = head;
		for (int i = 1; i < body.size(); i++)
		{
			auto tmp2 = body[i];
			body[i] = tmp;
			tmp = tmp2;
		}
		body[0].first += xp;
		body[0].second += yp;
	}
	else
	{
		body.back().first += xp;
		body.back().second += yp;
	}
}
