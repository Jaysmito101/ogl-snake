#include "UI.h"
#include "../State.h"
#include "GridRenderer.h"
#include "SnakeGame.h"

UI::UI(State* state)
{
	this->state = state;
}

UI::~UI()
{
}

void UI::Render()
{
	ImGui::Begin("Game Stats");

	ImGui::DragFloat("Scale", &state->scale, 0.05f);
	ImGui::DragFloat3("Offset", state->position, 0.05f);

	static int gridSize = 16;
	if (ImGui::InputInt("Grid Size", &gridSize))
	{
		state->gridSize = gridSize;
		state->gridRenderer->Resize(gridSize);
		state->snakeGame->Reset();
	}

	ImGui::Text("Deltatime : %f", state->deltatime);
	ImGui::Text("Is Playing: %s", (state->snakeGame->IsPlaying() ? "Yes" : "No"));
	ImGui::NewLine();
	ImGui::Text("Fruit At  : %d, %d", state->snakeGame->fruit.first, state->snakeGame->fruit.second);
	ImGui::Text("Score     : %d", state->snakeGame->GetScore());
	ImGui::Text("Lifespan  : %d", state->snakeGame->GetLifespan());

	if(ImGui::Button("Reset Game"))
	{
		state->snakeGame->Reset();
	}

	ImGui::DragFloat("Frame Delta", &state->frameDelta, 0.005f);

	ImGui::End();
}