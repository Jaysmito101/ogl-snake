#pragma once

class GridRenderer;
class UI;
class SnakeGame;
class FrameBuffer;

/*
* Stores the current state of the application
*/
struct State
{
	GridRenderer* gridRenderer = nullptr;
	UI* ui = nullptr;
	SnakeGame* snakeGame = nullptr;
	FrameBuffer* framebuffer = nullptr;

	// TEMP
	float scale = 1.0f;
	float position[3] = {0.0f};

	float deltatime = 1.0f;
	int gridSize = 16;
	float mouse[4] = {0.0f};

	float frameDelta = 0.5f;
};
